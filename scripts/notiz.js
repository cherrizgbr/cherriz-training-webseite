function init() {
	$.ajax({
		url: "sources/notiz.php",
		type: 'GET',
		dataType: 'json',
		success: function(result) {
			for (var i = 0; i < result.length; i++) {
				loadNotiz(result[i]);
			}
		},
		error: function(xhqr, text) {alert(text);}
	});
}

function loadNotiz(id) {
	$.ajax({
		url: "sources/notiz.php?id=" + id,
		type: 'GET',
		dataType: 'json',
		success: function(result) {
			$.get("template/notiz.html", function(data) {
				data = data.replace("##ID##", result.id);
				data = data.replace("##Titel##", result.titel);
				data = data.replace("##Text##", result.text);
				$("#notizen").append(data);
			});
		},
		error: function(xhqr, text) {alert(text);}
	});
}

function createNotiz(titel, text) {
	$.ajax({
		url: "sources/notiz.php",
		type: 'POST',
		dataType: 'json',
		data: {
			titel: titel,
			text: text
		},
		success: function(result) {
			loadNotiz(result.id);
		},
		error: function(xhqr, text) {alert(text);}
	});
}