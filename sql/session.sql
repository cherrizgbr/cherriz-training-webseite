CREATE TABLE Session
(
  ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  Session VARCHAR(100) NOT NULL
);
ALTER TABLE Session ADD CONSTRAINT unique_ID UNIQUE (ID);