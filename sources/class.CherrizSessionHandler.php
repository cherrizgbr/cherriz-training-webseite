<?php

class CherrizSessionHandler {

	/** Property der ID der aktuellen Sitzung */
	const NAME_SESSIONID = "SessionID";

	private $sessionDuration;

	private $sessionLife;

	private $rememberDuration;

	public function __construct() {
		$this->sessionDuration = 900;
		$this->rememberDuration = 2419200;
		$this->sessionLife = time() - $this->sessionDuration;

		// Den Session Handler auf die Methoden dieser Klasse setzen.
		session_set_save_handler(array($this, '_open'), array($this, '_close'), array($this, '_read'), array($this, '_write'),
		                         array($this, '_destroy'), array($this, '_gc'));

		session_start();

		register_shutdown_function('session_write_close');
	}

	public function _open() {
		return true;
	}

	public function _read($sessionID) {
		if (isset($_COOKIE[CherrizSessionHandler::NAME_SESSIONID]) && $_COOKIE[CherrizSessionHandler::NAME_SESSIONID] != $sessionID) {
			//Session in Cookie gesetzt, daraus übernehmen
			$sessionID = $_COOKIE[CherrizSessionHandler::NAME_SESSIONID];
			session_id($sessionID);
		}

		//Cookie, welches die Session ID speichert aktualisieren
		setcookie(CherrizSessionHandler::NAME_SESSIONID, $sessionID, time() + $this->rememberDuration, "/");

		return "";
	}

	public function _write($sessionID, $data) {
		// Statement, um eine bestehende Sitzung zu aktualisieren.
		$result = DBConnect::getDBConnection()->query("SELECT COUNT(*) AS 'Exists' FROM Session WHERE Session = '$sessionID'");
		$resObj = $result->fetch_object();
		if ($resObj->Exists == 0) {
			//Andernfalls muss eine neue Session erstellt werden
			DBConnect::getDBConnection()->query("INSERT INTO Session (Session) VALUES ('$sessionID')");
		}
		return true;
	}

	public function _close() {
		//Ruft den Garbage Collector auf
		$this->_gc(0);
		return true;
	}

	public function _destroy($sessionID) {
		$result = DBConnect::getDBConnection()->query("DELETE FROM Session WHERE Session = '$sessionID'");
		if ($result === true) {
			$_SESSION = array();
		}
		return $result;
	}

	public function _gc() {
		return true;
	}

}