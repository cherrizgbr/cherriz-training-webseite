<?php
//Fehleranzeige
error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", 1);
//seit PHP 5.3 sollte die Zeitzone gesetzt werden
date_default_timezone_set('Europe/Berlin');
//Eigenen SessionHandler einbinden
require("class.CherrizSessionHandler.php");
$GLOBALS['SESSION_HANDLER'] = new CherrizSessionHandler();
//Weitere Imports
require("class.DBConnect.php");
require("class.Notiz.php");

//Header
header('Content-Type: application/json');

$methode = $_SERVER['REQUEST_METHOD'];

switch ($methode) {
	case "GET":
		$id = $_GET["id"];
		if ($id != "") {
			$notiz = Notiz::createFromDB($id);
			echo("{\"id\": " . $notiz->getID() . ", \"titel\": \"" . $notiz->getTitel() . "\", \"text\": \"" . $notiz->getText() . "\"}");
		} else {
			$notizen = Notiz::getAll(session_id());
			echo("[" . implode(",", $notizen) . "]");
		}
		break;
	case "POST":
		$notiz = Notiz::create(session_id(), $_POST["titel"], $_POST["text"]);
		echo("{\"id\": " . $notiz->getID() . ", \"titel\": \"" . $notiz->getTitel() . "\", \"text\": \"" . $notiz->getText() . "\"}");
		break;
}