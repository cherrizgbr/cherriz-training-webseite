<?php

class Notiz {

	private $notiz;

	/**
	 * @param string $sessionID
	 *
	 * @return array
	 */
	public static function getAll($sessionID) {
		$notizen = array();
		$result = DBConnect::getDBConnection()->query("SELECT ID FROM Notiz WHERE Session = (SELECT ID FROM Session WHERE Session = '$sessionID')");
		if ($result) {
			while ($notiz = $result->fetch_object()) {
				array_push($notizen, $notiz->ID);
			}
		}
		return $notizen;
	}

	/**
	 * @param int $id
	 *
	 * @return Notiz
	 */
	public static function createFromDB($id) {
		$result = DBConnect::getDBConnection()->query("SELECT ID, Titel, Text FROM Notiz WHERE ID = $id");
		return new Notiz($result->fetch_object());
	}

	/**
	 * @param string $sessionID
	 * @param string $title
	 * @param string $text
	 *
	 * @return Notiz
	 */
	public static function create($sessionID, $title, $text) {
		DBConnect::getDBConnection()->query("INSERT INTO Notiz (Session, Titel, Text) VALUES ((SELECT ID FROM Session WHERE Session = '$sessionID'), '$title', '$text')");
		return Notiz::createFromDB(DBConnect::getDBConnection()->insert_id);
	}

	private function __construct($notiz) {
		$this->notiz = $notiz;
	}

	public function getID() {
		return $this->notiz->ID;
	}

	public function getTitel() {
		return DBConnect::getDBConnection()->real_escape_string($this->notiz->Titel);
	}

	public function getText() {
		return DBConnect::getDBConnection()->real_escape_string($this->notiz->Text);
	}

}