<?php

/**
 * Die Klasse verwaltet die Verbindung zur Datenbank.
 * @author Frederik Kirsch
 */
class DBConnect {

	const CHARSET_DEFAULT = "noCharset";

	const CHARSET_UTF8 = "utf8";

	const CHARSET_UTF8_BIN = "utf8_bin";

	const CHARSET_LATIN1_SWEDISH_CI = "latin1_swedish_ci";

	private static $db = null;

	private function __construct() {
	}

	/**
	 * @return MySQLi Liefert eine Verbindung zur Datenbank.
	 */
	public static function getDBConnection() {
		if (DBConnect::$db == null) {
			DBConnect::$db = DBConnect::createConnection(DBConnect::CHARSET_UTF8);
		}
		return DBConnect::$db;
	}

	/**
	 * Erzeugt eine Verbindung zur Datenbank.
	 *
	 * @param string $charset Der gewuenschte Zeichensatz.
	 *
	 * @return MySQLi Verbindung zur Datenbank
	 */
	private static function createConnection($charset) {
		$con = new MySQLi("localhost", "%SQL_USER%", "%SQL_PASS%", "%SQL_DB%");
		if (!$con) {
			die("Keine Verbindung zur Datenbank m�glich.");
		} else {
			if ($charset != DBConnect::CHARSET_DEFAULT) {
				$con->query("SET NAMES '" . $charset . "'");
				$con->query("SET CHARACTER SET '" . $charset . "'");
			}
		}
		return $con;
	}

}