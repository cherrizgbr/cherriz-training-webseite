<?php
//Fehleranzeige
error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", 1);
//seit PHP 5.3 sollte die Zeitzone gesetzt werden
date_default_timezone_set('Europe/Berlin');
//Eigenen SessionHandler einbinden
require("sources/class.DBConnect.php");
require("sources/class.CherrizSessionHandler.php");
$GLOBALS['SESSION_HANDLER'] = new CherrizSessionHandler();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8" content="text/html"/>
	<title>Cherriz Training - Todo</title>
	<link rel="stylesheet" type="text/css" href="styles/main.css">
	<script type='text/javascript' src='scripts/jquery-2.1.4.js'></script>
	<script type='text/javascript' src='scripts/notiz.js'></script>
	<script type='text/javascript' src='scripts/scripts.js'></script>
</head>
<body>
<h1>Cherriz Training - Notiz</h1>

<div id="create" class="box">
	<p>Neue Notiz zur erstellen:</p>

	<p>Aktive Session: <?php echo session_id() ?></p>

	<p>Titel <input type="text" name="titel" maxlength="100" title="title"/></p>

	<p>Text <textarea rows="4" cols="25" name="text" title="text"></textarea></p>
	<input type="button" id="btnCreate" value="Erstellen"/>
</div>
<div id="notizen">
</div>
</body>
</html>